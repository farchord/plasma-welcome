# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-welcome package.
#
# SPDX-FileCopyrightText: 2023 Mincho Kondarev <mkondarev@yahoo.de>
msgid ""
msgstr ""
"Project-Id-Version: plasma-welcome\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-29 01:38+0000\n"
"PO-Revision-Date: 2023-12-11 12:04+0100\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Минчо Кондарев"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "mkondarev@yahoo.de"

#: src/contents/ui/footers/FooterDefault.qml:29
#, kde-format
msgctxt "@action:button"
msgid "&Skip"
msgstr "Пр&опускане"

#: src/contents/ui/footers/FooterDefault.qml:29
#, kde-format
msgctxt "@action:button"
msgid "&Back"
msgstr "На&зад"

#: src/contents/ui/footers/FooterDefault.qml:62
#, kde-format
msgctxt "@action:button"
msgid "&Finish"
msgstr "З&авършване"

#: src/contents/ui/footers/FooterDefault.qml:62
#, kde-format
msgctxt "@action:button"
msgid "&Next"
msgstr "&Напред"

#: src/contents/ui/footers/FooterUpdate.qml:20
#, kde-format
msgctxt "@option:check"
msgid "Show this page after Plasma is updated"
msgstr "Показване на това съобщение след актуализация на Plasma"

#: src/contents/ui/footers/FooterUpdate.qml:30
#, kde-format
msgctxt "@action:button"
msgid "&OK"
msgstr "&Добре"

#: src/contents/ui/pages/Contribute.qml:16
#, kde-format
msgctxt "@title:window"
msgid "Get Involved"
msgstr "Включете се"

#: src/contents/ui/pages/Contribute.qml:17
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"KDE is not a big company; it's an international volunteer community, and its "
"software is made by people like you donating their time and passion. You too "
"can help make things better, whether it's through translation, visual "
"design, and promotion, or programming and bug triaging.<nl/><nl/>By "
"contributing to KDE, you can become part of something special, meet new "
"friends, learn new skills, and make a difference to millions while working "
"with people from around the globe to deliver a stunning free software "
"computing experience."
msgstr ""
"KDE не е голяма компания, а международна доброволческа общност, която се "
"създава от хора като вас, които даряват времето и страстта си. Вие също "
"можете да помогнете за подобряването на нещата, независимо дали става дума "
"за превод, визуално оформление, популяризиране или програмиране и "
"отстраняване на грешки.<nl/><nl/>Като допринасяте за KDE, можете да станете "
"част от нещо специално, да се запознаете с нови приятели, да усвоите нови "
"умения и да промените живота на милиони хора, докато работите с хора от цял "
"свят, за да предоставите зашеметяващ свободен софтуер и компютърно "
"изживяване."

#: src/contents/ui/pages/Contribute.qml:23
#, kde-format
msgctxt "@action:button"
msgid "Start Contributing!"
msgstr "Дайте Вашия принос!"

#: src/contents/ui/pages/Contribute.qml:44 src/contents/ui/pages/Donate.qml:44
#: src/contents/ui/pages/Welcome.qml:100
#, kde-format
msgctxt "@action:button clicking on this takes the user to a web page"
msgid "Visit %1"
msgstr "Посетете %1"

#: src/contents/ui/pages/Discover.qml:21
#, kde-format
msgctxt "@title:window"
msgid "Manage Software"
msgstr "Управление на програми"

#: src/contents/ui/pages/Discover.qml:22
#, kde-kuit-format
msgctxt ""
"@info:usagetip %1 is 'Discover', the name of KDE's software center app"
msgid ""
"The <application>%1</application> app helps you find and install "
"applications, games, and tools. You can search or browse by category, look "
"at screenshots, and read reviews to help you find the perfect app."
msgstr ""
"<application>%1</application> ви помага да намерите и инсталирате "
"приложения, игри и инструменти. Можете да търсите или разглеждате по "
"категории и да погледнете снимки на екрана и да прочетете ревютата, за да ви "
"помогнат да изберете перфектното приложение."

#: src/contents/ui/pages/Discover.qml:103
#, kde-format
msgctxt "@action:button %1 is the name of an app"
msgid "Show %1 in Discover"
msgstr "Показване на %1 в Discover"

#: src/contents/ui/pages/Donate.qml:15
#, kde-format
msgctxt "@title:window"
msgid "Support Your Freedom"
msgstr "Подкрепете свободата"

#: src/contents/ui/pages/Donate.qml:16
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"The KDE community relies on donations of expertise and funds, and is "
"supported by KDE e.V.--a German nonprofit that manages legal matters, funds "
"development sprints and server resources, and offers employment "
"opportunities for KDE developers. Donations to KDE e.V. support the wider "
"KDE community, and you can make a difference by donating today!<nl/><nl/"
">Donations are tax-deductible in Germany."
msgstr ""
"Общността на KDE разчита на дарения на средства и експертни знания. Тя се "
"поддържа от KDE e.V, -- немска организация с нестопанска цел, която "
"управлява правни въпроси, финансира състезания за разработки и сървърни "
"ресурси и предлага възможности за работа за разработчици на KDE. Даренията "
"към KDE e.V. поддържа широката общност на KDE.  и можете да направите "
"промяна, като дарите днес!<nl/><nl/>Даренията подлежат на приспадане на "
"данъци в Германия."

#: src/contents/ui/pages/Donate.qml:22
#: src/contents/ui/pages/plasmaupdate/Update.qml:74
#, kde-format
msgctxt "@action:button"
msgid "Make a donation"
msgstr "Направете дарение"

#: src/contents/ui/pages/Donate.qml:51
#, kde-format
msgctxt "@action:button"
msgid "Plasma 6 Supporters"
msgstr "Лица, подкрепящи Plasma 6"

#: src/contents/ui/pages/donate/Supporters.qml:16
#, kde-format
msgctxt "@title:window"
msgid "Plasma 6 Supporters"
msgstr "Лица, подкрепящи Plasma 6"

#: src/contents/ui/pages/donate/Supporters.qml:17
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"We thank the following people for supporting us during the Plasma 6 "
"fundraising campaign:"
msgstr ""
"Благодарим на следните хора, които ни подкрепиха по време на кампания за "
"набиране на средства за Plasma 6:"

#: src/contents/ui/pages/kcms/Feedback.qml:15
#, kde-format
msgctxt "@title: window"
msgid "Share Anonymous Usage Information"
msgstr "Анонимно споделяне за начина на използване системата"

#: src/contents/ui/pages/kcms/Feedback.qml:16
#, kde-format
msgctxt "@info:usagetip"
msgid ""
"Our developers will use this anonymous data to improve KDE software. You can "
"choose how much to share in System Settings, and here too."
msgstr ""
"Нашите разработчици ще използват тези анонимни данни, за да подобрят "
"софтуера на KDE. Вие можете да да изберете в каква степен те да бъдат "
"споделени в Системни настройки, както и тук."

#: src/contents/ui/pages/Live.qml:19
#, kde-format
msgctxt "@title:window %1 is the name of the user's distro"
msgid "Welcome to %1!"
msgstr "Добре дошли в %1!"

#: src/contents/ui/pages/Live.qml:22
#, kde-kuit-format
msgctxt "@info:usagetip %1 is the name of the user's distro"
msgid ""
"Pressing the icon below will begin installing %1. Alternatively, you can "
"close the window to explore the live environment or continue here to find "
"out about KDE Plasma."
msgstr ""
"Натискането на иконата по-долу ще започне инсталирането на %1. Като "
"алтернатива можете да затворите прозореца, за да разгледате средата на живо "
"или да продължите тук, за да получите информация за KDE Plasma."

#: src/contents/ui/pages/Live.qml:24
#, kde-kuit-format
msgctxt "@info:usagetip %1 is the name of the user's distro"
msgid ""
"You can close the window to explore the live environment or continue here to "
"find out about KDE Plasma."
msgstr ""
"Можете да затворите прозореца, за да разгледате средата на живо или да "
"продължите тук, за да получите информация за KDE Plasma."

#: src/contents/ui/pages/Live.qml:30
#, kde-format
msgctxt "@action:button"
msgid "Visit home page"
msgstr "Домашна страница"

#: src/contents/ui/pages/Network.qml:23
#, kde-format
msgctxt "@info:window"
msgid "Access the Internet"
msgstr "Свързване с интернет"

#: src/contents/ui/pages/Network.qml:24
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"You can connect to the internet and manage your network connections with the "
"<interface>Networks applet</interface>. To access it, click on the "
"<interface>Networks</interface> icon in your <interface>System Tray</"
"interface>, which lives in the bottom-right corner of the screen."
msgstr ""
"Можете да се свържете с интернет и да управлявате мрежовите си връзки с "
"помощта на <интерфейс>Мрежи</интерфейс>. За да получите достъп до него, "
"щракнете върху <интерфейс>Мрежи</интерфейс> в <интерфейс>Системна лента</"
"интерфейс>.интерфейс>, която се намира в долния десен ъгъл на екрана."

#: src/contents/ui/pages/Network.qml:248
#, kde-format
msgctxt "@info"
msgid ""
"This page is being shown regardless of network connectivity because you are "
"using a development version."
msgstr ""
"Тази страница се показва независимо от мрежовата свързаност, защото "
"използвате версия за разработчици."

#: src/contents/ui/pages/PlasmaUpdate.qml:18
#, kde-format
msgctxt "@title:window"
msgid "Plasma has been updated to %1"
msgstr "Plasma е актуализирана до версия %1"

#: src/contents/ui/pages/plasmaupdate/Beta.qml:17
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"Thank you for testing this beta release of Plasma—your feedback is "
"fundamental to helping us improve it! Please report any and all bugs you "
"find so that we can fix them."
msgstr ""
"Благодарим ви, че тествате тази бета версия на Plasma - вашите отзиви са за "
"да ни помогнат да я подобрим! Моля, докладвайте за всички грешки, които "
"намерите, за да можем да ги отстраним."

#: src/contents/ui/pages/plasmaupdate/Beta.qml:23
#: src/contents/ui/pages/plasmaupdate/Update.qml:24
#, kde-format
msgctxt "@action:button"
msgid "<b>Find out what's new</b>"
msgstr "<b>Вижте новостите</b>"

#: src/contents/ui/pages/plasmaupdate/Beta.qml:28
#, kde-format
msgctxt "@action:button"
msgid "Report a bug"
msgstr "Сигнал за грешка"

#: src/contents/ui/pages/plasmaupdate/Beta.qml:32
#: src/contents/ui/pages/plasmaupdate/Update.qml:29
#, kde-format
msgctxt "@action:button"
msgid "Help work on the next release"
msgstr "Помогнете в създаването на следващото издание"

#: src/contents/ui/pages/plasmaupdate/Update.qml:18
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"KDE contributors have spent the last four months hard at work on this "
"release. We hope you enjoy using Plasma as much as we enjoyed making it!"
msgstr ""
"Сътрудниците на KDE прекараха последните четири месеца в усилена работа по "
"това издание. Надяваме се, че ще се радвате да използвате Plasma толкова, "
"колкото ние се радвахме на създаването ѝ!"

#: src/contents/ui/pages/plasmaupdate/Update.qml:70
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"The KDE community relies on donations of expertise and funds, and is "
"supported by KDE e.V.—a German nonprofit. Donations to KDE e.V. support the "
"wider KDE community, and you can make a difference by donating today."
msgstr ""
"Общността на KDE разчита на дарения на средства и експертни знания. Тя се "
"поддържа от KDE e.V, -- немска организация с нестопанска цел.  Даренията към "
"KDE e.V. поддържа широката общност на KDE.  Вие можете да направите промяна, "
"като дарите днес."

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:19
#, kde-format
msgctxt "@info:window"
msgid "Powerful When Needed"
msgstr "Използване на пълната мощност на системата, когато е необходимо"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:20
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"Plasma is an extremely feature-rich environment, designed to super-charge "
"your productivity! Here is just a smattering of the things it can do for you:"
msgstr ""
"Plasma е изключително богата на функции среда, създадена да подобри "
"значително вашата продуктивност! Ето само малка част от нещата, които тя "
"може да направи за вас:"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:64
#, kde-format
msgctxt "@title:row Short form of the 'Vaults' Plasma feature"
msgid "Vaults"
msgstr "Трезори"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:65
#, kde-format
msgctxt "@info Caption for Plasma Vaults button"
msgid "Store sensitive files securely"
msgstr "Сигурно съхраняване на файловете"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:74
#, kde-format
msgctxt "@title:row Name of the 'Activities' Plasma feature"
msgid "Activities"
msgstr "Дейности"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:75
#, kde-format
msgctxt ""
"@info Caption for Activities button. Note that 'Separate' is being used as "
"an imperative verb here, not a noun."
msgid "Separate work, school, or home tasks"
msgstr "Разделяне на служебни, училищни или домашни задачи"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:84
#, kde-format
msgctxt "@title:row Name of the 'KDE Connect' feature"
msgid "KDE Connect"
msgstr "KDE Connect"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:85
#, kde-format
msgctxt "@info Caption for KDE Connect button"
msgid "Connect your phone and your computer"
msgstr "Свързване на вашия телефон и компютър"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:96
#, kde-format
msgctxt "@title:row"
msgid "KRunner"
msgstr "KRunner"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:97
#, kde-format
msgctxt "@info Caption for KRunner button"
msgid "Search for anything"
msgstr "Неограничено търсене"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:106
#, kde-format
msgctxt "@title:row Name of the 'Overview' KWin effect"
msgid "Overview"
msgstr "Преглед"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:107
#, kde-format
msgctxt "@info Caption for Overview button"
msgid "Your system command center"
msgstr "Вашият системен команден център"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:116
#, kde-format
msgctxt "@title:row"
msgid "Get New Stuff"
msgstr "Изтегляне на нови неща"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:117
#, kde-format
msgctxt "@info Caption for Get New Stuff button"
msgid "Extend the system with add-ons"
msgstr "Разширяване на системата с добавки"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:124
#, kde-format
msgctxt "@action:button"
msgid "Learn about more Plasma features"
msgstr "Научете повече за възможностите на Plasma"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:133
#, kde-format
msgctxt "@info:window"
msgid "Plasma Vaults"
msgstr "Трезор на Plasma"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:134
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"Plasma Vaults allows you to create encrypted folders, called "
"<interface>Vaults.</interface> Inside each Vault, you can securely store "
"your passwords, files, pictures, and documents, safe from prying eyes. "
"Vaults can live inside folders that are synced to cloud storage services "
"too, providing extra privacy for that content.<nl/><nl/>To get started, "
"click the arrow on the <interface>System Tray</interface> to show hidden "
"items, and then click the <interface>Vaults</interface> icon."
msgstr ""
"Трезор на Plasma ви позволява да създавате криптирани папки, наречени "
"<interface>Трезори.</interface> Във всяко от тях можете да съхранявате "
"сигурно паролите си, файловете, снимките и документите си, защитени от "
"любопитни очи. Трезорите могат да се намират в папки, които са "
"синхронизирани с услуги за съхранение в облак също така, осигурявайки "
"допълнителна поверителност за това съдържание.<nl/><nl/>За да започнете, "
"щракнете върху стрелката на <interface>Системната област</interface>, за да "
"покажете скритите елементи, а след това щракнете върху иконата "
"<interface>Трезори</interface>."

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:142
#, kde-format
msgctxt "@info:window"
msgid "Activities"
msgstr "Дейности"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:143
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"Activities can be used to separate high-level projects or workflows so you "
"can focus on one at a time. You can have an activity for \"Home\", \"School"
"\", \"Work\", and so on. Each Activity has access to all your files but has "
"its own set of open apps and windows, recent documents, \"Favorite\" apps, "
"and desktop widgets.<nl/><nl/>To get started, launch <interface>System "
"Settings</interface> and search for \"Activities\". On that page, you can "
"create more Activities. You can then switch between them using the "
"<shortcut>Meta+Tab</shortcut> keyboard shortcut."
msgstr ""
"Дейностите могат да се използват за разделяне на проекти или работни процеси "
"на високо ниво, така че можете да се съсредоточите върху един или друг "
"проект в даден момент. Можете да имате дейност за \"Дом\", \"Училище\", "
"\"Работа\" и т.н. Всяка дейност има достъп до всички ваши файлове, но има "
"свой собствен набор от отворени приложения и прозорци, скорошни документи, "
"\"Предпочитани\" приложения, и уиджети на работния плот.<nl/><nl/>За да "
"започнете, стартирайте <interface>Системни настройки</interface> и потърсете "
"\"Дейности\". На тази страница можете да да създадете нови дейности. След "
"това можете да превключвате между тях с помощта на клавишната комбинация "
"<shortcut>Meta+Tab</shortcut>."

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:148
#: src/contents/ui/pages/PowerfulWhenNeeded.qml:179
#, kde-format
msgctxt "@action:button"
msgid "Open Settings…"
msgstr "Отваряне на панела с настройки…"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:159
#, kde-format
msgctxt "@info:window"
msgid "KDE Connect"
msgstr "KDE Connect"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:163
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"KDE Connect lets you integrate your phone with your computer in various ways:"
"<nl/><list><item>See notifications from your phone on your computer</"
"item><item>Reply to text messages from your phone on your computer</"
"item><item>Sync your clipboard contents between your computer and your "
"phone</item><item>Make a noise on your phone when it's been misplaced</"
"item><item>Copy pictures, videos, and other files from your phone to your "
"computer, and vice versa</item><item>…And much more!</item></list><nl/>To "
"get started, launch <interface>System Settings</interface> and search for "
"\"KDE Connect\". On that page, you can pair your phone."
msgstr ""
"KDE Connect ви позволява да интегрирате телефона си с компютъра по различни "
"начини:<nl/><list><item>Показване на известията от телефона на компютъра си</"
"item><item>Отговаряне на текстови съобщения от телефона си на компютъра</"
"item><item>Синхронизиране на съдържанието на клипборда между компютъра и "
"телефона ви</item><item>Издаване на сигнал от телефона, когато не знаете "
"къде е</item><item>Копиране на снимки, видеоклипове и други файлове от "
"телефона в компютъра и обратно</item><item>...И още много други неща!</"
"item></list><nl/>За да да започнете, стартирайте <interface>Системни "
"настройки</interface> и потърсете \"KDE Connect\". На тази страница можете "
"да сдвоите телефона си."

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:190
#, kde-format
msgctxt "@info:window"
msgid "KRunner"
msgstr "KRunner"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:194
#, kde-kuit-format
msgctxt ""
"@info:usagetip translators: In the example queries, make sure to use the "
"keywords as they are localized in the actual runner plugins. If needed, "
"change 'Shanghai' to a city that on the other side of the world from likely "
"speakers of the language"
msgid ""
"KRunner is Plasma's exceptionally powerful and versatile search system. It "
"powers the search functionality in the Application Launcher menu and the "
"Overview screen, and it can be accessed as a standalone search bar using the "
"<shortcut>Alt+Space</shortcut> keyboard shortcut.<nl/><nl/>In addition to "
"finding your files and folders, KRunner can launch apps, search the web, "
"convert between currencies, calculate math problems, and a lot more. Try "
"typing any of the following into one of those search fields:<nl/><list><item>"
"\"time Shanghai\"</item><item>\"27/3\"</item><item>\"200 EUR in USD\"</"
"item><item>\"25 miles in km\"</item><item>…And much more!</item></list><nl/"
">To learn more, open the KRunner search bar using the <shortcut>Alt+Space</"
"shortcut> keyboard shortcut and click on the question mark icon."
msgstr ""
"KRunner е изключително мощната и гъвкава система за търсене на Plasma. Тя "
"задвижва функциите за търсене в менюто за стартиране на приложения и в "
"екрана за общ преглед. Може да бъде използвана като самостоятелна лента за "
"търсене с помощта на <shortcut>Alt+Space</shortcut> клавишна комбинация.<nl/"
"><nl/>В допълнение към намиране на файлове и папки, KRunner може да стартира "
"приложения, да търси в интернет, да конвертира между валути, да изчислява "
"математически задачи и много други. Опитайте въведете някое от следните неща "
"в някое от тези полета за търсене:<nl/><list><item>\"време Шанхай\"</"
"item><item>\"27/3\"</item><item>\"200 EUR в USD\"</item><item>\"25 мили в км"
"\"</item><item>...И много други!</item></list><nl/>За да научите повече, "
"отворете лентата за търсене на KRunner, като използвате <shortcut>Alt+Space</"
"shortcut> и щракнете върху иконата с въпросителен знак."

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:210
#, kde-format
msgctxt "@info:window The name of a KWin effect"
msgid "Overview"
msgstr "Преглед"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:211
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"Overview is a full-screen overlay that shows all of your open windows, "
"letting you easily access any of them. It also shows your current Virtual "
"Desktops, allowing you to add more, remove some, and switch between them. "
"Finally, it offers a KRunner-powered search field that can also filter "
"through open windows.<nl/><nl/>You can access Overview using the "
"<shortcut>Meta+W</shortcut> keyboard shortcut."
msgstr ""
"Общ преглед е наслагване на цял екран, което показва всички отворени "
"прозорци, и ви позволява да имате лесен достъп до всеки от тях. Показва и "
"текущия ви виртуален десктоп, като ви позволява да добавяте нови, да "
"премахвате някои и да превключвате между тях. И накрая, той предлага поле за "
"търсене, управлявано от KRunner, което може да филтрира и през отворените "
"прозорци.<nl/><nl/>Можете да получите достъп до Преглед, като използвате "
"<shortcut>Meta+W</shortcut> клавишна комбинация."

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:219
#, kde-format
msgctxt "@info:window"
msgid "Get New Stuff"
msgstr "Изтегляне на нови неща"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:220
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"Throughout Plasma, System Settings, and KDE apps, you'll find buttons marked "
"\"Get New <emphasis>thing</emphasis>…\". Clicking on them will show you 3rd-"
"party content to extend the system, made by other people like you! In this "
"way, it is often possible to add functionality you want without having to "
"ask KDE developers to implement it themselves.<nl/><nl/>Note that content "
"acquired this way has not been reviewed by your distributor for "
"functionality or stability."
msgstr ""
"В Plasma, системните настройки и приложенията на KDE ще откриете бутони с "
"надпис \"Изтегляне на нови   <emphasis>неща </emphasis>...\". Щракването "
"върху тях ще ви покаже съдържание от трети страни, с което да разширите "
"системата, създадено от други хора като вас! По този начин често е възможно "
"да добавите желаната от вас функционалност, без да се налага да карате "
"разработчиците на KDE да я реализират.<nl/><nl/>Отбележете, че съдържанието, "
"придобито по този начин, не е прегледано от вашия дистрибутор за "
"функционалност или стабилност."

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:225
#, kde-format
msgctxt "@action:button"
msgid "See 3rd-Party Content…"
msgstr "Вижте наличните съдържания от трети страни..."

#: src/contents/ui/pages/SimpleByDefault.qml:16
#, kde-format
msgctxt "@info:window"
msgid "Simple by Default"
msgstr "Опростена по подразбиране"

#: src/contents/ui/pages/SimpleByDefault.qml:17
#, kde-kuit-format
msgctxt ""
"@info:usagetip %1 is either 'System Settings' or 'Plasma Settings', the "
"settings app for Plasma Desktop or Plasma Mobile"
msgid ""
"Plasma is designed to be simple and usable out of the box. Things are where "
"you'd expect, and there is generally no need to configure anything before "
"you can be comfortable and productive.<nl/><nl/>Should you feel the need to, "
"you'll find what you need in the <application>%1</application> app."
msgstr ""
"Plasma е проектирана така, че да бъде проста и да може да се използва "
"веднага след инсталирането. Нещата са там, където очаквате, и като цяло не е "
"необходимо да конфигурирате нищо, за да се чувствате удобно и продуктивно."
"<nl/><nl/>Ако имате нужда от промени, ще намерите необходимото в "
"приложението <application>%1</application>."

#: src/contents/ui/pages/Welcome.qml:17
#, kde-format
msgctxt "@title"
msgid "Welcome"
msgstr "Добре дошли"

#: src/contents/ui/pages/Welcome.qml:19
#, kde-kuit-format
msgctxt "@info:usagetip %1 is custom text supplied by the distro"
msgid ""
"%1<nl/><nl/>This operating system is running Plasma, a free and open-source "
"desktop environment created by KDE, an international software community of "
"volunteers. It is designed to be simple by default for a smooth experience, "
"but powerful when needed to help you really get things done. We hope you "
"love it!"
msgstr ""
"%1<nl/><nl/>Тази операционна система използва KDE Plasma. Plasma е безплатна "
"среда за настолни компютри с отворен код, създадена от KDE, международна "
"софтуерна общност от доброволци. Работната среда на Plasma е проста по "
"подразбиране, за да работи безпроблемно, но е мощна и комплексна, когато е "
"необходимо, за да ви помогне наистина  да свършите нещо.<nl/>Надяваме се, че "
"ще я харесате!"

#: src/contents/ui/pages/Welcome.qml:20
#, kde-kuit-format
msgctxt "@info:usagetip %1 is the name of the user's distro"
msgid ""
"Welcome to the %1 operating system running KDE Plasma!<nl/><nl/>Plasma is a "
"free and open-source desktop environment created by KDE, an international "
"software community of volunteers. It is designed to be simple by default for "
"a smooth experience, but powerful when needed to help you really get things "
"done. We hope you love it!"
msgstr ""
"Добре дошли в %1, използваща KDE Plasma. Plasma е безплатна среда за "
"настолни компютри с отворен код, създадена от KDE, международна софтуерна "
"общност от доброволци. Работната среда на Plasma е проста по подразбиране, "
"за да работи безпроблемно, но е мощна и комплексна, когато е необходимо, за "
"да ви помогне наистина  да свършите нещо.<nl/>Надяваме се, че ще я харесате!"

#: src/contents/ui/pages/Welcome.qml:29
#, kde-format
msgctxt "@action:button"
msgid "Learn more about the KDE community"
msgstr "Научете повече за общността на KDE"

#: src/contents/ui/pages/Welcome.qml:34
#, kde-format
msgctxt "@action:button %1 is the name of the user's distro"
msgid "Learn more about %1"
msgstr "Научете повече за %1"

#: src/contents/ui/pages/Welcome.qml:72
#, kde-format
msgctxt "@title"
msgid "Image loading failed"
msgstr "Неуспешно зареждане на изображението"

#: src/contents/ui/pages/Welcome.qml:73
#, kde-kuit-format
msgctxt "@info:placeholder"
msgid "Could not load <filename>%1</filename>. Make sure it exists."
msgstr ""
"Неуспешно зареждане на <filename>%1</filename>. Уверете се,  че е налично. "

#: src/contents/ui/pages/Welcome.qml:107
#, kde-format
msgctxt "@info"
msgid "The KDE mascot Konqi welcomes you to the KDE community!"
msgstr "Змейчето Konqi ви приветства с добре дошли в общността на KDE!"

#: src/controller.cpp:54
#, kde-format
msgid "6.0 Dev"
msgstr "6.0 Dev"

#: src/controller.cpp:57
#, kde-format
msgid "6.0 Alpha"
msgstr "6.0 Alpha"

#: src/controller.cpp:60
#, kde-format
msgid "6.0 Beta 1"
msgstr "6.0 Beta 1"

#: src/controller.cpp:63
#, kde-format
msgid "6.0 Beta 2"
msgstr "6.0 Beta 2"

#: src/controller.cpp:66
#, kde-format
msgctxt "@label RC meaning Release Candidate"
msgid "6.0 RC1"
msgstr "6.0 RC1"

#: src/controller.cpp:69
#, kde-format
msgctxt "@label RC meaning Release Candidate"
msgid "6.0 RC2"
msgstr "6.0 RC2"

#: src/controller.cpp:87
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "%1 Dev"
msgstr "%1 Dev"

#: src/controller.cpp:91
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "%1 Beta"
msgstr "%1 Beta"

#: src/controller.cpp:94
#, kde-format
msgctxt "@label %1 is the Plasma version, %2 is the beta release number"
msgid "%1 Beta %2"
msgstr "%1 Beta %2"

#: src/main.cpp:40
#, kde-format
msgctxt "@info:usagetip"
msgid "A welcome app for KDE Plasma"
msgstr "Програма за приветстване и начални настройки на KDE Plasma"

#: src/main.cpp:45
#, kde-format
msgctxt "@title"
msgid "Welcome Center"
msgstr "Добре дошли"

#: src/main.cpp:53
#, kde-format
msgctxt "@info copyright string"
msgid "(c) 2021"
msgstr "(c) 2021"

#: src/main.cpp:54
#, kde-format
msgctxt "@info:credit"
msgid "Felipe Kinoshita"
msgstr "Felipe Kinoshita"

#: src/main.cpp:55 src/main.cpp:59
#, kde-format
msgctxt "@info:credit"
msgid "Author"
msgstr "Автор"

#: src/main.cpp:58
#, kde-format
msgctxt "@info:credit"
msgid "Nate Graham"
msgstr "Nate Graham"

#: src/main.cpp:71
#, kde-format
msgid "Display release notes for the current Plasma release."
msgstr "Показване на бележките на текущото издание на Plasma."

#: src/main.cpp:72
#, kde-format
msgid ""
"Display release notes for the current Plasma release, for beta versions."
msgstr "Показване на бележките на текущото издание на Plasma, за бета версии."

#: src/main.cpp:73
#, kde-format
msgid "Display the live page intended for distro live environments."
msgstr ""
"Показване на страницата, предвидена за средата на живо на дистрибуцията. "

#: src/qml/ApplicationIcon.qml:39
#, kde-format
msgctxt "@action:button"
msgid "Launch %1 now"
msgstr "Стартиране на %1 сега"
