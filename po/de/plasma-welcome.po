# German translations for plasma-welcome package.
# Copyright (C) 2022 This file is copyright:
# This file is distributed under the same license as the plasma-welcome package.
# Jannick Kuhr <opensource@kuhr.org>, 2023.
# Frederik Schwarzer <schwarzer@kde.org>, 2023.
# SPDX-FileCopyrightText: 2023 Johannes Obermayr <johannesobermayr@gmx.de>
#
# Automatically generated, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-welcome\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-29 01:38+0000\n"
"PO-Revision-Date: 2023-12-21 18:35+0100\n"
"Last-Translator: Johannes Obermayr <johannesobermayr@gmx.de>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.08.4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Deutsches KDE-Übersetzerteam"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kde-i18n-de@kde.org"

#: src/contents/ui/footers/FooterDefault.qml:29
#, kde-format
msgctxt "@action:button"
msgid "&Skip"
msgstr "Über&springen"

#: src/contents/ui/footers/FooterDefault.qml:29
#, kde-format
msgctxt "@action:button"
msgid "&Back"
msgstr "&Zurück"

#: src/contents/ui/footers/FooterDefault.qml:62
#, kde-format
msgctxt "@action:button"
msgid "&Finish"
msgstr "Be&enden"

#: src/contents/ui/footers/FooterDefault.qml:62
#, kde-format
msgctxt "@action:button"
msgid "&Next"
msgstr "&Weiter"

#: src/contents/ui/footers/FooterUpdate.qml:20
#, kde-format
msgctxt "@option:check"
msgid "Show this page after Plasma is updated"
msgstr "Diese Seite nach dem Aktualisieren von Plasma zeigen"

#: src/contents/ui/footers/FooterUpdate.qml:30
#, kde-format
msgctxt "@action:button"
msgid "&OK"
msgstr "&OK"

#: src/contents/ui/pages/Contribute.qml:16
#, kde-format
msgctxt "@title:window"
msgid "Get Involved"
msgstr "Machen Sie mit"

#: src/contents/ui/pages/Contribute.qml:17
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"KDE is not a big company; it's an international volunteer community, and its "
"software is made by people like you donating their time and passion. You too "
"can help make things better, whether it's through translation, visual "
"design, and promotion, or programming and bug triaging.<nl/><nl/>By "
"contributing to KDE, you can become part of something special, meet new "
"friends, learn new skills, and make a difference to millions while working "
"with people from around the globe to deliver a stunning free software "
"computing experience."
msgstr ""
"KDE ist kein Großunternehmen sondern eine internationale Gemeinschaft von "
"Freiwilligen. Die Software wird von Menschen wie Ihnen gemacht, die dem "
"Projekt ihre Freizeit und Leidenschaft widmen. Auch Sie können helfen die "
"Dinge noch besser zu machen, sei es durch Übersetzungen, Beiträge zum "
"Design, Werbung, Programmierung oder bei der Fehlerbehebung.<nl/><nl/>Durch "
"Ihren Beitrag zu KDE können Sie Teil von etwas Besonderem werden, neue "
"Freunde kennenlernen, Fähigkeiten erlernen und das Leben von Millionen "
"verändern, indem Sie mit Menschen aus der ganzen Welt zusammenarbeiten, um "
"ein großartiges freies Software-Erlebnis zu ermöglichen."

#: src/contents/ui/pages/Contribute.qml:23
#, kde-format
msgctxt "@action:button"
msgid "Start Contributing!"
msgstr "Jetzt mitmachen!"

#: src/contents/ui/pages/Contribute.qml:44 src/contents/ui/pages/Donate.qml:44
#: src/contents/ui/pages/Welcome.qml:100
#, kde-format
msgctxt "@action:button clicking on this takes the user to a web page"
msgid "Visit %1"
msgstr "%1 besuchen"

#: src/contents/ui/pages/Discover.qml:21
#, kde-format
msgctxt "@title:window"
msgid "Manage Software"
msgstr "Software verwalten"

#: src/contents/ui/pages/Discover.qml:22
#, kde-kuit-format
msgctxt ""
"@info:usagetip %1 is 'Discover', the name of KDE's software center app"
msgid ""
"The <application>%1</application> app helps you find and install "
"applications, games, and tools. You can search or browse by category, look "
"at screenshots, and read reviews to help you find the perfect app."
msgstr ""
"<application>%1</application> hilft Ihnen dabei, Anwendungen, Spiele und "
"Werkzeuge zu finden und zu installieren. Sie können suchen oder durch "
"Kategorien blättern sowie Bildschirmfotos ansehen und Bewertungen lesen, um "
"die für Sie beste Anwendung zu finden."

#: src/contents/ui/pages/Discover.qml:103
#, kde-format
msgctxt "@action:button %1 is the name of an app"
msgid "Show %1 in Discover"
msgstr "%1 in Discover anzeigen"

#: src/contents/ui/pages/Donate.qml:15
#, kde-format
msgctxt "@title:window"
msgid "Support Your Freedom"
msgstr "Unterstützen Sie Ihre Freiheit"

#: src/contents/ui/pages/Donate.qml:16
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"The KDE community relies on donations of expertise and funds, and is "
"supported by KDE e.V.--a German nonprofit that manages legal matters, funds "
"development sprints and server resources, and offers employment "
"opportunities for KDE developers. Donations to KDE e.V. support the wider "
"KDE community, and you can make a difference by donating today!<nl/><nl/"
">Donations are tax-deductible in Germany."
msgstr ""
"Die KDE-Gemeinschaft stützt sich auf Spenden von Fachwissen und Geldern und "
"wird vom gemeinnützigen Verein KDE e. V. unterstützt, der sich um rechtliche "
"Fragen, Entwicklungssprints und Serverressourcen kümmert sowie "
"Beschäftigungsmöglichkeiten für KDE-Entwickler bietet. Spenden an den KDE e. "
"V. unterstützen die breitere KDE-Gemeinschaft und mit Ihrer heutigen Spende "
"können Sie einen Unterschied machen.<nl/><nl/>Spenden sind in Deutschland in "
"der Regel steuerlich absetzbar."

#: src/contents/ui/pages/Donate.qml:22
#: src/contents/ui/pages/plasmaupdate/Update.qml:74
#, kde-format
msgctxt "@action:button"
msgid "Make a donation"
msgstr "Etwas spenden"

#: src/contents/ui/pages/Donate.qml:51
#, kde-format
msgctxt "@action:button"
msgid "Plasma 6 Supporters"
msgstr "Unterstützer von Plasma 6"

#: src/contents/ui/pages/donate/Supporters.qml:16
#, kde-format
msgctxt "@title:window"
msgid "Plasma 6 Supporters"
msgstr "Unterstützer von Plasma 6"

#: src/contents/ui/pages/donate/Supporters.qml:17
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"We thank the following people for supporting us during the Plasma 6 "
"fundraising campaign:"
msgstr ""
"Wir danken den folgenden Personen für die Unterstützung während der "
"Spendenkampagne für Plasma 6:"

#: src/contents/ui/pages/kcms/Feedback.qml:15
#, kde-format
msgctxt "@title: window"
msgid "Share Anonymous Usage Information"
msgstr "Anonyme Nutzungsstatistiken übermitteln"

#: src/contents/ui/pages/kcms/Feedback.qml:16
#, kde-format
msgctxt "@info:usagetip"
msgid ""
"Our developers will use this anonymous data to improve KDE software. You can "
"choose how much to share in System Settings, and here too."
msgstr ""
"Unsere Entwickler werden diese anonymen Daten zur Verbesserung der KDE-"
"Software nutzen. Sie können den Umfang der Datenübermittlung in den "
"Systemeinstellungen und auch hier festlegen."

#: src/contents/ui/pages/Live.qml:19
#, kde-format
msgctxt "@title:window %1 is the name of the user's distro"
msgid "Welcome to %1!"
msgstr "Willkommen zu %1!"

#: src/contents/ui/pages/Live.qml:22
#, fuzzy, kde-kuit-format
#| msgctxt "@info:usagetip %1 is the name of the user's distro"
#| msgid ""
#| "Pressing the icon below will begin installing %1. Alternatively, you can "
#| "close the window to explore the live environment or continue here to find "
#| "out about KDE Plasma."
msgctxt "@info:usagetip %1 is the name of the user's distro"
msgid ""
"Pressing the icon below will begin installing %1. Alternatively, you can "
"close the window to explore the live environment or continue here to find "
"out about KDE Plasma."
msgstr ""
"Durch den Klick auf das Symbol unterhalb wird die Installation von %1 "
"gestartet. Sie können auch das Fenster zum Entdecken der Echtzeitumgebung "
"schließen oder für weitere Informationen zu KDE Plasma hier fortfahren."

#: src/contents/ui/pages/Live.qml:24
#, fuzzy, kde-kuit-format
#| msgctxt "@info:usagetip %1 is the name of the user's distro"
#| msgid ""
#| "You can close the window to explore the live environment or continue here "
#| "to find out about KDE Plasma."
msgctxt "@info:usagetip %1 is the name of the user's distro"
msgid ""
"You can close the window to explore the live environment or continue here to "
"find out about KDE Plasma."
msgstr ""
"Sie können das Fenster zum Entdecken der Echtzeitumgebung schließen oder für "
"weitere Informationen zu KDE Plasma hier fortfahren."

#: src/contents/ui/pages/Live.qml:30
#, kde-format
msgctxt "@action:button"
msgid "Visit home page"
msgstr "Webseite besuchen"

#: src/contents/ui/pages/Network.qml:23
#, kde-format
msgctxt "@info:window"
msgid "Access the Internet"
msgstr "Auf das Internet zugreifen"

#: src/contents/ui/pages/Network.qml:24
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"You can connect to the internet and manage your network connections with the "
"<interface>Networks applet</interface>. To access it, click on the "
"<interface>Networks</interface> icon in your <interface>System Tray</"
"interface>, which lives in the bottom-right corner of the screen."
msgstr ""
"Sie können sich mit dem Internet verbinden und Ihre Netzwerkverbindungen mit "
"dem Miniprogramm <interface>Netzwerke</interface> verwalten. Um darauf "
"zuzugreifen, klicken Sie auf das <interface>Netzwerke</interface>-Symbol im "
"<interface>Systemabschnitt der Kontrollleiste</interface>, welchen Sie am "
"unteren rechten Rand Ihres Bildschirms finden."

#: src/contents/ui/pages/Network.qml:248
#, kde-format
msgctxt "@info"
msgid ""
"This page is being shown regardless of network connectivity because you are "
"using a development version."
msgstr ""
"Diese Seite wird unabhängig von einer aktiven Netzwerkverbindung gezeigt, da "
"Sie eine Entwicklungsversion verwenden."

#: src/contents/ui/pages/PlasmaUpdate.qml:18
#, kde-format
msgctxt "@title:window"
msgid "Plasma has been updated to %1"
msgstr "Plasma wurde zu %1 aktualisiert"

#: src/contents/ui/pages/plasmaupdate/Beta.qml:17
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"Thank you for testing this beta release of Plasma—your feedback is "
"fundamental to helping us improve it! Please report any and all bugs you "
"find so that we can fix them."
msgstr ""
"Vielen Dank für das Testen dieser Betaversion von Plasma — Ihre Rückmeldung "
"ist für die weitere Verbesserung elementar. Bitte melden Sie deshalb alle "
"Fehler, die Sie finden, damit wir sie beheben können."

#: src/contents/ui/pages/plasmaupdate/Beta.qml:23
#: src/contents/ui/pages/plasmaupdate/Update.qml:24
#, kde-format
msgctxt "@action:button"
msgid "<b>Find out what's new</b>"
msgstr "<b>Erfahren Sie die Neuerungen</b>"

#: src/contents/ui/pages/plasmaupdate/Beta.qml:28
#, kde-format
msgctxt "@action:button"
msgid "Report a bug"
msgstr "Probleme oder Wünsche berichten"

#: src/contents/ui/pages/plasmaupdate/Beta.qml:32
#: src/contents/ui/pages/plasmaupdate/Update.qml:29
#, kde-format
msgctxt "@action:button"
msgid "Help work on the next release"
msgstr "Bei der Arbeit an der nächsten Veröffentlichung helfen"

#: src/contents/ui/pages/plasmaupdate/Update.qml:18
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"KDE contributors have spent the last four months hard at work on this "
"release. We hope you enjoy using Plasma as much as we enjoyed making it!"
msgstr ""
"Die an KDE Mitwirkenden haben in den letzten vier Monaten harte Arbeit in "
"diese Veröffentlichung investiert. Wir hoffen, dass Sie soviel Spaß an der "
"Verwendung von Plasma haben werden, wie wir an der Entwicklung hatten!"

#: src/contents/ui/pages/plasmaupdate/Update.qml:70
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"The KDE community relies on donations of expertise and funds, and is "
"supported by KDE e.V.—a German nonprofit. Donations to KDE e.V. support the "
"wider KDE community, and you can make a difference by donating today."
msgstr ""
"Die KDE-Gemeinschaft stützt sich auf Spenden von Fachwissen und Geldern und "
"wird vom deutschen gemeinnützigen Verein KDE e. V. unterstützt. Spenden an "
"den KDE e. V. unterstützen die breitere KDE-Gemeinschaft und mit Ihrer "
"heutigen Spende können Sie einen Unterschied machen."

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:19
#, kde-format
msgctxt "@info:window"
msgid "Powerful When Needed"
msgstr "Mächtig bei Bedarf"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:20
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"Plasma is an extremely feature-rich environment, designed to super-charge "
"your productivity! Here is just a smattering of the things it can do for you:"
msgstr ""
"Plasma ist eine extrem funktionsreiche Arbeitsumgebung, wie dafür gemacht, "
"Ihre Produktivität zu optimieren. Hier nur ein paar wenige Beispiel, was "
"Plasma für Sie tun kann:"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:64
#, kde-format
msgctxt "@title:row Short form of the 'Vaults' Plasma feature"
msgid "Vaults"
msgstr "Tresore"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:65
#, kde-format
msgctxt "@info Caption for Plasma Vaults button"
msgid "Store sensitive files securely"
msgstr "Sensible Dateien sicher speichern"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:74
#, kde-format
msgctxt "@title:row Name of the 'Activities' Plasma feature"
msgid "Activities"
msgstr "Aktivitäten"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:75
#, kde-format
msgctxt ""
"@info Caption for Activities button. Note that 'Separate' is being used as "
"an imperative verb here, not a noun."
msgid "Separate work, school, or home tasks"
msgstr "Arbeit, Schule und private Aufgaben trennen"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:84
#, kde-format
msgctxt "@title:row Name of the 'KDE Connect' feature"
msgid "KDE Connect"
msgstr "KDE Connect"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:85
#, kde-format
msgctxt "@info Caption for KDE Connect button"
msgid "Connect your phone and your computer"
msgstr "Ihr Telefon mit Ihrem Rechner verbinden"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:96
#, kde-format
msgctxt "@title:row"
msgid "KRunner"
msgstr "KRunner"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:97
#, kde-format
msgctxt "@info Caption for KRunner button"
msgid "Search for anything"
msgstr "Suchen und Finden"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:106
#, kde-format
msgctxt "@title:row Name of the 'Overview' KWin effect"
msgid "Overview"
msgstr "Übersicht"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:107
#, kde-format
msgctxt "@info Caption for Overview button"
msgid "Your system command center"
msgstr "Die Kommandozentrale Ihres Systems"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:116
#, kde-format
msgctxt "@title:row"
msgid "Get New Stuff"
msgstr "Neue Erweiterungen herunterladen"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:117
#, kde-format
msgctxt "@info Caption for Get New Stuff button"
msgid "Extend the system with add-ons"
msgstr "Ihr System mit Erweiterungen personalisieren"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:124
#, kde-format
msgctxt "@action:button"
msgid "Learn about more Plasma features"
msgstr "Erfahren Sie mehr über weitere Plasma-Funktionen"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:133
#, kde-format
msgctxt "@info:window"
msgid "Plasma Vaults"
msgstr "Plasma Tresore"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:134
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"Plasma Vaults allows you to create encrypted folders, called "
"<interface>Vaults.</interface> Inside each Vault, you can securely store "
"your passwords, files, pictures, and documents, safe from prying eyes. "
"Vaults can live inside folders that are synced to cloud storage services "
"too, providing extra privacy for that content.<nl/><nl/>To get started, "
"click the arrow on the <interface>System Tray</interface> to show hidden "
"items, and then click the <interface>Vaults</interface> icon."
msgstr ""
"Plasma Tresore ermöglichen es Ihnen, verschlüsselte Ordner, genannt "
"<interface>Tresore</interface>, zu erstellen. In jedem Tresor können Sie "
"Passwörter, Dateien, Bilder und Dokumente vor neugierigen Augen geschützt "
"hinterlegen. Tresore können auch innerhalb von mit Cloud-Speichern "
"synchronisierten Ordnern angelegt werden und so zusätzliche Privatsphäre für "
"Ihre Inhalte schaffen.<nl/><nl/> Um zu beginnen, klicken Sie zunächst auf "
"den Pfeil im <interface>Systemabschnitt der Kontrollleiste</interface>, um "
"die ausgeblendeten Elemente anzuzeigen, und dann auf das <interface>Tresore</"
"interface>-Symbol."

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:142
#, kde-format
msgctxt "@info:window"
msgid "Activities"
msgstr "Aktivitäten"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:143
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"Activities can be used to separate high-level projects or workflows so you "
"can focus on one at a time. You can have an activity for \"Home\", \"School"
"\", \"Work\", and so on. Each Activity has access to all your files but has "
"its own set of open apps and windows, recent documents, \"Favorite\" apps, "
"and desktop widgets.<nl/><nl/>To get started, launch <interface>System "
"Settings</interface> and search for \"Activities\". On that page, you can "
"create more Activities. You can then switch between them using the "
"<shortcut>Meta+Tab</shortcut> keyboard shortcut."
msgstr ""
"Aktivitäten können dazu verwendet werden, spezialisierte Projekte oder "
"Arbeitsabläufe von einander zu trennen, so dass Sie fokussiert bleiben. Sie "
"können zum Beispiel jeweils eine Aktivität für „Privates”, „Schule“, "
"„Arbeit“ usw. anlegen. Jede Aktivität lässt Sie auf alle Ihre Dateien "
"zugreifen, hat aber eine eigene Zusammenstellung an geöffneten Fenstern, "
"zuletzt verwendeten Dokumenten, bevorzugten Anwendungen und Miniprogrammen "
"auf der Arbeitsfläche.<nl/><nl/>Um zu beginnen, öffnen Sie die "
"<interface>Systemeinstellungen</interface> und suchen Sie nach "
"„Aktivitäten“. Auf dieser Seite können Sie beliebig viele weitere "
"Aktivitäten anlegen. Um zwischen ihnen zu wechseln, verwenden Sie den "
"Tastaturkurzbefehl <shortcut>Meta+Tab</shortcut>."

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:148
#: src/contents/ui/pages/PowerfulWhenNeeded.qml:179
#, kde-format
msgctxt "@action:button"
msgid "Open Settings…"
msgstr "Einstellungen öffnen ..."

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:159
#, kde-format
msgctxt "@info:window"
msgid "KDE Connect"
msgstr "KDE Connect"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:163
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"KDE Connect lets you integrate your phone with your computer in various ways:"
"<nl/><list><item>See notifications from your phone on your computer</"
"item><item>Reply to text messages from your phone on your computer</"
"item><item>Sync your clipboard contents between your computer and your "
"phone</item><item>Make a noise on your phone when it's been misplaced</"
"item><item>Copy pictures, videos, and other files from your phone to your "
"computer, and vice versa</item><item>…And much more!</item></list><nl/>To "
"get started, launch <interface>System Settings</interface> and search for "
"\"KDE Connect\". On that page, you can pair your phone."
msgstr ""
"KDE Connect ermöglicht es Ihrem Telefon und Ihrem Rechner auf vielfältige "
"Art und Weise miteinander zu interagieren:<nl/><list><item>Sehen Sie "
"Benachrichtigungen von Ihrem Telefon auf Ihrem Rechner</"
"item><item>Beantworten Sie Textnachrichten von Ihrem Telefon auf Ihrem "
"Rechner</item><item>Synchronisieren Sie Inhalte der Zwischenablage zwischen "
"Ihrem Telefon und Ihrem Rechner</item><item>Spielen Sie einen Signalton auf "
"Ihrem Telefon ab, wenn Sie es verlegt haben</item><item>Kopieren Sie Bilder, "
"Videos und andere Dateien von Ihrem Telefon auf Ihren Rechner und "
"andersherum</item><item>…Und vieles mehr!</item></list><nl/>Um zu beginnen, "
"öffnen Sie die <interface>Systemeinstellungen</interface> und suchen Sie "
"nach „KDE Connect“. Auf dieser Seite können Sie Ihr Telefon koppeln."

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:190
#, kde-format
msgctxt "@info:window"
msgid "KRunner"
msgstr "KRunner"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:194
#, kde-kuit-format
msgctxt ""
"@info:usagetip translators: In the example queries, make sure to use the "
"keywords as they are localized in the actual runner plugins. If needed, "
"change 'Shanghai' to a city that on the other side of the world from likely "
"speakers of the language"
msgid ""
"KRunner is Plasma's exceptionally powerful and versatile search system. It "
"powers the search functionality in the Application Launcher menu and the "
"Overview screen, and it can be accessed as a standalone search bar using the "
"<shortcut>Alt+Space</shortcut> keyboard shortcut.<nl/><nl/>In addition to "
"finding your files and folders, KRunner can launch apps, search the web, "
"convert between currencies, calculate math problems, and a lot more. Try "
"typing any of the following into one of those search fields:<nl/><list><item>"
"\"time Shanghai\"</item><item>\"27/3\"</item><item>\"200 EUR in USD\"</"
"item><item>\"25 miles in km\"</item><item>…And much more!</item></list><nl/"
">To learn more, open the KRunner search bar using the <shortcut>Alt+Space</"
"shortcut> keyboard shortcut and click on the question mark icon."
msgstr ""
"KRunner ist Plasmas außergewöhnlich leistungsstarkes und vielseitiges "
"Suchsystem. Es stellt die Suchfunktion im Anwendungsstarter und dem "
"Überblick-Bildschirm zur Verfügung, kann aber als eigenständige Suchleiste "
"über den Tastaturkurzbefehl <shortcut>Alt+Leertaste</shortcut> aufgerufen "
"werden.<nl/><nl/>Neben dem Auffinden von Dateien und Ordnern kann KRunner "
"auch Anwendungen starten, im Internet suchen, Währungen umrechnen, "
"mathematische Probleme lösen und vieles mehr. Versuchen Sie, eines der "
"folgenden Beispiele in ein Suchfeld einzugeben:<nl/><list><item>\"zeit "
"Shanghai\"</item><item>\"27/3\"</item><item>\"200 EUR in USD\"</item><item>"
"\"25 Meilen in km\"</item><item>Und vieles mehr!</item></list><nl/>Um mehr "
"zu erfahren, öffnen Sie die KRunner-Suchleiste mit dem Tastaturkurzbefehl "
"<shortcut>Alt+Leertaste</shortcut> und klicken Sie auf das Fragezeichen-"
"Symbol."

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:210
#, kde-format
msgctxt "@info:window The name of a KWin effect"
msgid "Overview"
msgstr "Übersicht"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:211
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"Overview is a full-screen overlay that shows all of your open windows, "
"letting you easily access any of them. It also shows your current Virtual "
"Desktops, allowing you to add more, remove some, and switch between them. "
"Finally, it offers a KRunner-powered search field that can also filter "
"through open windows.<nl/><nl/>You can access Overview using the "
"<shortcut>Meta+W</shortcut> keyboard shortcut."
msgstr ""
"„Übersicht“ ist eine Vollbildansicht, die Ihnen alle offenen Fenster zeigt "
"und jedes einzelne einfach zugänglich macht. Sie zeigt Ihnen außerdem Ihre "
"aktuelle virtuelle Arbeitsfläche und ermöglicht es Ihnen, weitere "
"hinzuzufügen, zu entfernen oder zwischen ihnen zu wechseln. Zusätzlich wird "
"ein KRunner-Suchfeld angezeigt, mit dem Sie die offenen Fenster filtern "
"können.<nl/><nl/>Sie können die „Übersicht“ mit dem Tastaturkurzbefehl "
"<shortcut>Meta+W</shortcut> öffnen."

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:219
#, kde-format
msgctxt "@info:window"
msgid "Get New Stuff"
msgstr "Neue Erweiterungen herunterladen"

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:220
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"Throughout Plasma, System Settings, and KDE apps, you'll find buttons marked "
"\"Get New <emphasis>thing</emphasis>…\". Clicking on them will show you 3rd-"
"party content to extend the system, made by other people like you! In this "
"way, it is often possible to add functionality you want without having to "
"ask KDE developers to implement it themselves.<nl/><nl/>Note that content "
"acquired this way has not been reviewed by your distributor for "
"functionality or stability."
msgstr ""
"Überall in Plasma, z. B. in den Systemeinstellungen oder in KDE-Anwendungen, "
"finden Sie Knöpfe mit der Aufschrift „Neue <emphasis>Dinge</emphasis> "
"holen“. Wenn Sie darauf klicken, werden Ihnen Erweiterungen für Ihr System "
"angezeigt, die von Menschen wie Ihnen gemacht wurden. Auf diese Weise ist es "
"oft möglich, Funktionen nachzurüsten, ohne KDE-Entwickler darum bitten zu "
"müssen.<nl/><nl/>Beachten Sie, dass auf diesem Wege bezogene Inhalte von "
"Ihrer Distribution nicht auf Funktion oder Stabilität geprüft wurden."

#: src/contents/ui/pages/PowerfulWhenNeeded.qml:225
#, kde-format
msgctxt "@action:button"
msgid "See 3rd-Party Content…"
msgstr "Inhalte von Dritten ansehen ..."

#: src/contents/ui/pages/SimpleByDefault.qml:16
#, kde-format
msgctxt "@info:window"
msgid "Simple by Default"
msgstr "Von Haus aus einfach"

#: src/contents/ui/pages/SimpleByDefault.qml:17
#, kde-kuit-format
msgctxt ""
"@info:usagetip %1 is either 'System Settings' or 'Plasma Settings', the "
"settings app for Plasma Desktop or Plasma Mobile"
msgid ""
"Plasma is designed to be simple and usable out of the box. Things are where "
"you'd expect, and there is generally no need to configure anything before "
"you can be comfortable and productive.<nl/><nl/>Should you feel the need to, "
"you'll find what you need in the <application>%1</application> app."
msgstr ""
"Plasma wurde so gestaltet, dass es ohne weiteren Aufwand einfach "
"funktioniert. Die Dinge sind da, wo Sie sie erwarten, und es besteht keine "
"Notwendigkeit irgendetwas aufwendig einzurichten, bevor Sie bequem und "
"produktiv arbeiten können.<nl/><nl/>Sollten Sie doch etwas anpassen wollen, "
"finden Sie es in den <application>%1</application>."

#: src/contents/ui/pages/Welcome.qml:17
#, kde-format
msgctxt "@title"
msgid "Welcome"
msgstr "Willkommen"

#: src/contents/ui/pages/Welcome.qml:19
#, kde-kuit-format
msgctxt "@info:usagetip %1 is custom text supplied by the distro"
msgid ""
"%1<nl/><nl/>This operating system is running Plasma, a free and open-source "
"desktop environment created by KDE, an international software community of "
"volunteers. It is designed to be simple by default for a smooth experience, "
"but powerful when needed to help you really get things done. We hope you "
"love it!"
msgstr ""
"%1<nl/><nl/>Plasma ist eine von KDE, einer internationalen Gemeinschaft von "
"Freiwilligen, geschaffene freie quelloffene Arbeitsumgebung. Die Plasma-"
"Arbeitsumgebung ist standardmäßig einfach gehalten, aber ausgesprochen "
"leistungsstark, wenn große Dinge erledigt werden wollen. Wir hoffen, dass "
"Sie sie lieben werden!"

#: src/contents/ui/pages/Welcome.qml:20
#, kde-kuit-format
msgctxt "@info:usagetip %1 is the name of the user's distro"
msgid ""
"Welcome to the %1 operating system running KDE Plasma!<nl/><nl/>Plasma is a "
"free and open-source desktop environment created by KDE, an international "
"software community of volunteers. It is designed to be simple by default for "
"a smooth experience, but powerful when needed to help you really get things "
"done. We hope you love it!"
msgstr ""
"Willkommen beim %1-Betriebssystem mit verwendetem KDE Plasma.<nl/><nl/"
">Plasma ist eine freie und quelloffene Arbeitsumgebung von KDE, einer "
"internationalen Gemeinschaft von Freiwilligen. Es ist standardmäßig einfach "
"gehalten, aber ausgesprochen leistungsstark, wenn große Dinge erledigt "
"werden wollen. Wir hoffen, dass Sie sie lieben werden!"

#: src/contents/ui/pages/Welcome.qml:29
#, kde-format
msgctxt "@action:button"
msgid "Learn more about the KDE community"
msgstr "Erfahren Sie mehr über die KDE-Gemeinschaft"

#: src/contents/ui/pages/Welcome.qml:34
#, kde-format
msgctxt "@action:button %1 is the name of the user's distro"
msgid "Learn more about %1"
msgstr "Mehr über %1 erfahren"

#: src/contents/ui/pages/Welcome.qml:72
#, kde-format
msgctxt "@title"
msgid "Image loading failed"
msgstr "Das Laden des Bildes ist fehlgeschlagen"

#: src/contents/ui/pages/Welcome.qml:73
#, kde-kuit-format
msgctxt "@info:placeholder"
msgid "Could not load <filename>%1</filename>. Make sure it exists."
msgstr ""
"<filename>%1</filename> lässt sich nicht laden. Vergewissern Sie sich, dass "
"die Datei existiert."

#: src/contents/ui/pages/Welcome.qml:107
#, kde-format
msgctxt "@info"
msgid "The KDE mascot Konqi welcomes you to the KDE community!"
msgstr "Das KDE-Maskottchen Konqi begrüßt Sie in der KDE-Gemeinschaft!"

#: src/controller.cpp:54
#, kde-format
msgid "6.0 Dev"
msgstr "6.0 Dev"

#: src/controller.cpp:57
#, kde-format
msgid "6.0 Alpha"
msgstr "6.0 Alpha"

#: src/controller.cpp:60
#, kde-format
msgid "6.0 Beta 1"
msgstr "6.0 Beta 1"

#: src/controller.cpp:63
#, kde-format
msgid "6.0 Beta 2"
msgstr "6.0 Beta 2"

#: src/controller.cpp:66
#, kde-format
msgctxt "@label RC meaning Release Candidate"
msgid "6.0 RC1"
msgstr "6.0 RC1"

#: src/controller.cpp:69
#, kde-format
msgctxt "@label RC meaning Release Candidate"
msgid "6.0 RC2"
msgstr "6.0 RC2"

#: src/controller.cpp:87
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "%1 Dev"
msgstr "%1 Dev"

#: src/controller.cpp:91
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "%1 Beta"
msgstr "%1 Beta"

#: src/controller.cpp:94
#, kde-format
msgctxt "@label %1 is the Plasma version, %2 is the beta release number"
msgid "%1 Beta %2"
msgstr "%1 Beta %2"

#: src/main.cpp:40
#, kde-format
msgctxt "@info:usagetip"
msgid "A welcome app for KDE Plasma"
msgstr "Ein Begrüßungsassistent für KDE Plasma"

#: src/main.cpp:45
#, kde-format
msgctxt "@title"
msgid "Welcome Center"
msgstr "Begrüßungsassistent"

#: src/main.cpp:53
#, kde-format
msgctxt "@info copyright string"
msgid "(c) 2021"
msgstr "(c) 2021"

#: src/main.cpp:54
#, kde-format
msgctxt "@info:credit"
msgid "Felipe Kinoshita"
msgstr "Felipe Kinoshita"

#: src/main.cpp:55 src/main.cpp:59
#, kde-format
msgctxt "@info:credit"
msgid "Author"
msgstr "Autor"

#: src/main.cpp:58
#, kde-format
msgctxt "@info:credit"
msgid "Nate Graham"
msgstr "Nate Graham"

#: src/main.cpp:71
#, kde-format
msgid "Display release notes for the current Plasma release."
msgstr "Hinweise zur aktuellen Plasma-Veröffentlichung anzeigen."

#: src/main.cpp:72
#, kde-format
msgid ""
"Display release notes for the current Plasma release, for beta versions."
msgstr ""
"Hinweise zur aktuellen Plasma-Veröffentlichung bei Betaversionen anzeigen."

#: src/main.cpp:73
#, kde-format
msgid "Display the live page intended for distro live environments."
msgstr ""

#: src/qml/ApplicationIcon.qml:39
#, kde-format
msgctxt "@action:button"
msgid "Launch %1 now"
msgstr "%1 jetzt starten"

#~ msgctxt "@title: window"
#~ msgid "Connect Online Accounts"
#~ msgstr "Online-Konten verbinden"

#~ msgctxt "@info:usagetip"
#~ msgid ""
#~ "This will let you access their content in KDE apps. You can set it up in "
#~ "System Settings, and here too."
#~ msgstr ""
#~ "Dies ermöglicht es Ihnen, auf die jeweiligen Inhalte in KDE-Anwendungen "
#~ "zuzugreifen. Sie können die Einrichtung in den Systemeinstellungen und "
#~ "auch hier durchführen."

#~ msgctxt "@info:usagetip"
#~ msgid ""
#~ "Miraculously, KDE operates on a shoestring budget, relying largely on "
#~ "donations of labor and resources. You can help change that!<nl/><nl/"
#~ ">Financial donations help KDE pay for development sprints, hardware and "
#~ "server resources, and expanded employment. Donations are tax-deductible "
#~ "in Germany."
#~ msgstr ""
#~ "Auf wundersame Weise schafft es KDE mit einem sehr knappen Budget "
#~ "auszukommen, größtenteils angewiesen auf Spenden von Ressourcen und "
#~ "Arbeitskraft. Auch Sie können Ihren Beitrag leisten!<nl/><nl/"
#~ ">Finanzspenden helfen KDE, Entwicklerkonferenzen, Hardware und Server "
#~ "sowie feste Mitarbeiter zu bezahlen. In Deutschland sind Zuwendungen "
#~ "zudem steuerlich absetzbar."

#~ msgctxt "@title"
#~ msgid "Welcome to KDE Plasma!"
#~ msgstr "Willkommen zu KDE Plasma!"

#~ msgctxt "@title the name of the app 'Discover'"
#~ msgid "Discover"
#~ msgstr "Discover"

#~ msgctxt "@action:button"
#~ msgid "Launch System Settings now"
#~ msgstr "Systemeinstellungen jetzt starten"

#~ msgctxt "@title the name of the 'System Settings' app"
#~ msgid "System Settings"
#~ msgstr "Systemeinstellungen"

#~ msgctxt "@action:button"
#~ msgid "See More Features"
#~ msgstr "Weitere Funktionen ansehen"

#~ msgctxt "@action:button"
#~ msgid "Open Activities Page in System Settings"
#~ msgstr "„Aktivitäten“-Seite in den Systemeinstellungen öffnen"

#~ msgctxt "@action:button"
#~ msgid "Open KDE Connect Page in System Settings"
#~ msgstr "KDE Connect in den Systemeinstellungen öffnen"

#~ msgctxt "@info"
#~ msgid "Page %1 of %2"
#~ msgid_plural "Page %1 of %2"
#~ msgstr[0] "Seite %1 von %2"
#~ msgstr[1] "Seite %1 von %2"

#~ msgctxt "@title"
#~ msgid "Welcome to KDE Plasma"
#~ msgstr "Willkommen zu KDE Plasma"

#~ msgid ""
#~ "Version number of the Plasma release to display release notes for, e.g. "
#~ "5.25"
#~ msgstr ""
#~ "In den Hinweisen zur Veröffentlichung anzuzeigende Versionsnummer der "
#~ "Plasma-Veröffentlichung, z. B. 5.25"
